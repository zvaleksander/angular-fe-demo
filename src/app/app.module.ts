import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { ClientIndexComponent } from './clients-page/client-index/client-index.component';
import { ClientFormComponent } from './clients-page/client-form/client-form.component';
import { HomeIndexComponent } from './home-page/home-index/home-index.component';

const routes: Routes = [
	{
		path: '',
		component: HomeIndexComponent
	},
	{
		path: 'clients',
		component: ClientIndexComponent
	},
	{
		path: 'clients/create',
		component: ClientFormComponent
	},
	{
		path: 'clients/:id/edit',
		component: ClientFormComponent
	}
]

@NgModule({
	declarations: [
		AppComponent,
		NavbarComponent,
		ClientIndexComponent,
		ClientFormComponent,
		HomeIndexComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		FormsModule,
		RouterModule.forRoot(routes),
		NgxPaginationModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
