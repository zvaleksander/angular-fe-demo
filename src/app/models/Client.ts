export class Client {
    id: number;
    document: string;
    name: string;
    lastname: string;
    email: string;
    address: string;
    phone: string;
}