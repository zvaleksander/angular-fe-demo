export class Pagination {
    total: number;
    current: number = 1;
    page: number = 0;
    size: number = 5;
}