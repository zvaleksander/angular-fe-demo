import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Client } from '../models/client';

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' });
    private endpoint: string = 'http://spring-clients-api.herokuapp.com/api/clients';

    constructor(private http: HttpClient) { }

    findAll(page: number, size: number): Observable<Client[]> {
        return this.http.get<Client[]>(this.endpoint + '?page=' + page + '&size=' + size);
    }

    findOne(id: number): Observable<any> {
        return this.http.get<any>(this.endpoint + '/' + id);
    }

    store(client: Client): Observable<Client> {
        return this.http.post<Client>(this.endpoint, client, { headers: this.httpHeaders });
    }

    update(id: number, client: Client): Observable<Client> {
        return this.http.put<Client>(this.endpoint + '/' + id, client, { headers: this.httpHeaders });
    }
}
