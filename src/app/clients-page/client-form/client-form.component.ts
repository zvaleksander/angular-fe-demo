import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';

import { ClientService } from '../../services/client.service';
import { Client } from 'src/app/models/client';

@Component({
    selector: 'app-client-form',
    templateUrl: './client-form.component.html',
    styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
    private id: number;

    public title = 'Registrar Cliente';
    public client: Client = new Client();

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private clientService: ClientService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(parameters => {
            this.id = parameters['id']
        });

        if (typeof this.id !== 'undefined') {
            this.title = 'Editar Cliente';

            this.clientService.findOne(this.id).subscribe(
                (instance) => this.client = instance
            )
        }
    }

    public save(): void {
        if (typeof this.id !== 'undefined') {
            this.clientService.update(this.id, this.client).subscribe(
                (instance) => {
                    this.router.navigateByUrl('/clients');
                    swal.fire('', 'El cliente se actualizó correctamente!', 'info');
                }
            );
        } else {
            this.clientService.store(this.client).subscribe(
                (instance) => {
                    this.router.navigateByUrl('/clients');
                    swal.fire('', 'El cliente se registro exitosamente!', 'success');
                }
            )
        }
    }
}
