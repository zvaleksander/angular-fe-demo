import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from 'src/app/models/Client';
import { Pagination } from 'src/app/models/Pagination';

@Component({
    selector: 'app-client-index',
    templateUrl: './client-index.component.html',
    styleUrls: ['./client-index.component.css']
})
export class ClientIndexComponent implements OnInit {

    public clients: Client[] = [];
    public pagination: Pagination = new Pagination();

    constructor(private clientService: ClientService) { }

    ngOnInit() {
        this.findAll(this.pagination.page, this.pagination.size);
    }

    findAll(page: number, size: number): void {
        this.clientService.findAll(page, size).subscribe(
            (collection: any) => {
                this.clients = collection.content;

                this.pagination.total = collection.totalElements;
                this.pagination.size = collection.size;
                this.pagination.page = collection.number;
            }
        )
    }

    reload(page: number): void {
        this.pagination.current = page;
        this.findAll(page - 1, this.pagination.size);
    }
}
